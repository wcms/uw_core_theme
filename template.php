<?php

/**
 * @file
 * Here we override the default HTML output of drupal.
 */

/**
 * Implements theme_process_html_tag().
 *
 * Remove unneccessary attributes from 'script' and 'style' elements.
 */
function uw_core_theme_process_html_tag(&$variables) {
  $element =& $variables['element'];

  if (in_array($element['#tag'], ['script', 'style'], TRUE)) {
    // Remove @type.
    unset($element['#attributes']['type']);

    // Remove @media="all".
    if ($element['#tag'] === 'style' && isset($element['#attributes']['media']) && $element['#attributes']['media'] === 'all') {
      unset($element['#attributes']['media']);
    }
  }
}

/**
 * Implements theme_preprocess_html().
 *
 * Add tabs.css if the user is logged in.
 */
function uw_core_theme_preprocess_html(&$variables) {
  if (user_is_logged_in()) {
    drupal_add_css(drupal_get_path('theme', 'uw_core_theme') . '/css/tabs.css');
  }
  // Serialize RDF Namespaces into an RDFa 1.1 prefix attribute.
  // See Zen theme: http://drupalcode.org/project/zen.git/commitdiff/34afaf5
  if ($variables['rdf_namespaces']) {
    $prefixes = array();
    foreach (explode("\n  ", ltrim($variables['rdf_namespaces'])) as $namespace) {
      // Remove xlmns: and ending quote and fix prefix formatting.
      $prefixes[] = str_replace('="', ': ', substr($namespace, 6, -1));
    }
    $variables['rdf_namespaces'] = ' prefix="' . implode(' ', $prefixes) . '"';
  }
  // Add defined suffix to all page titles. Default to University.
  if (isset($variables['head_title'])) {
    $variables['head_title'] .= check_plain(variable_get('uw_theme_title_append', ' | University of Waterloo'));
  }
}

/**
 * Implements template_preprocess_page().
 */
function uw_core_theme_preprocess_page(&$vars) {
  if (isset($vars['node_title'])) {
    $vars['title'] = $vars['node_title'];
  }
  // Adding classes wether #navigation is here or not.
  if (!empty($vars['main_menu']) or !empty($vars['sub_menu'])) {
    $vars['classes_array'][] = 'with-navigation';
  }
  if (!empty($vars['secondary_menu'])) {
    $vars['classes_array'][] = 'with-subnav';
  }
}

/**
 * Implements template_preprocess_node().
 */
function uw_core_theme_preprocess_node(&$vars) {
  // Add a striping class.
  $vars['classes_array'][] = 'node-' . $vars['zebra'];
}

/**
 * Implements template_preprocess_block().
 */
function uw_core_theme_preprocess_block(&$vars) {
  // Add a striping class.
  $vars['classes_array'][] = 'block-' . $vars['zebra'];
}

/**
 * Converts a string to a suitable html ID attribute.
 *
 * The HTML specification specifies what makes a valid ID attribute in HTML.
 * http://www.w3.org/TR/html4/struct/global.html#h-7.5.2
 * This function:
 *   - Ensure an ID starts with an alpha character by optionally adding an 'n'.
 *   - Replaces any character except A-Z, numbers, and underscores with dashes.
 *   - Converts entire string to lowercase.
 *
 * @param string $string
 *   The string.
 *
 * @return string
 *   The converted string.
 */
function uw_core_theme_id_safe($string) {
  // Replace with dashes anything that isn't A-Z, numbers, dashes, or underscores.
  $string = strtolower(preg_replace('/[^a-zA-Z0-9_-]+/', '-', $string));
  // If the first character is not a-z, add 'n' in front.
  // Don't use ctype_alpha since its locale aware.
  if (!ctype_lower($string[0])) {

    $string = 'id' . $string;
  }
  return $string;
}

/**
 * Implements theme_menu_link().
 */
function uw_core_theme_menu_link(array $variables) {
  $element = $variables['element'];
  $sub_menu = '';

  if ($element['#below']) {
    $sub_menu = drupal_render($element['#below']);
  }
  $output = l($element['#title'], $element['#href'], $element['#localized_options']);
  // Adding a class depending on the TITLE of the link (not constant).
  if ($element['#title']) {
    $element['#attributes']['class'][] = uw_core_theme_id_safe($element['#title']);
  }
  // Adding a class depending on the ID of the link (constant).
  $element['#attributes']['class'][] = 'mid-' . $element['#original_link']['mlid'];
  return '<li' . drupal_attributes($element['#attributes']) . '>' . $output . $sub_menu . "</li>\n";
}

/**
 * Implements theme_menu_local_task().
 */
function uw_core_theme_preprocess_menu_local_task(&$variables) {
  $link =& $variables['element']['#link'];

  // If the link does not contain HTML already, check_plain() it now.
  // After we set 'html'=TRUE the link will not be sanitized by l().
  if (empty($link['localized_options']['html'])) {
    $link['title'] = check_plain($link['title']);
  }
  $link['localized_options']['html'] = TRUE;
  $link['title'] = '<span class="tab">' . $link['title'] . '</span>';
}

/**
 * Implements theme_menu_local_tasks().
 *
 * Adds clearfix to tabs.
 */
function uw_core_theme_menu_local_tasks(&$variables) {
  $output = '';

  if (!empty($variables['primary'])) {
    $variables['primary']['#prefix'] = '<h2 class="element-invisible">' . t('Primary tabs') . '</h2>';
    $variables['primary']['#prefix'] .= '<ul class="tabs primary clearfix">';
    $variables['primary']['#suffix'] = '</ul>';
    $output .= drupal_render($variables['primary']);
  }
  if (!empty($variables['secondary'])) {
    $variables['secondary']['#prefix'] = '<h2 class="element-invisible">' . t('Secondary tabs') . '</h2>';
    $variables['secondary']['#prefix'] .= '<ul class="tabs secondary clearfix">';
    $variables['secondary']['#suffix'] = '</ul>';
    $output .= drupal_render($variables['secondary']);
  }

  return $output;
}

/**
 * Implements hook_theme().
 */
function uw_core_theme_theme() {
  return array(
    'global_menu' => array(
      'variables' => array(
        'items' => array(),
        'attributes' => array(),
      ),
    ),
  );
}

/**
 * Theming for global menus.
 *
 * A simplified version of theme_item_list().
 *
 * @param array $variables
 *   An associative array containing:
 *    - items: array of links.
 *    - attributes: attributes applied to the list element.
 *
 * @return string
 *   The output HTML.
 */
function uw_core_theme_global_menu(array $variables) {
  $links = $variables['items'];
  $attributes = $variables['attributes'];

  $output = '';
  if (!empty($links)) {
    $output = '<ul' . drupal_attributes($attributes) . '>';
    foreach ($links as $link) {
      $output .= '<li>' . $link . '</li>';
    }
    $output .= '</ul>';
  }
  return $output;
}

/**
 * Implements theme_status_messages().
 *
 * Hide certain messages.
 */
function uw_core_theme_status_messages($variables) {
  // Get all messages (which clears the list), set the messages again, ignoring what we don't want.
  foreach (drupal_get_messages() as $type => $messages) {
    if (is_array($messages)) {
      foreach ($messages as $message) {
        // Hide this message.
        if (preg_match('/^.*has been protected from the following editing operations:.*$/', $message)) {
          continue;
        }
        $message = _uw_core_theme_replace_with_cas_link($message);
        drupal_set_message($message, $type);
      }
    }
  }

  // Call the core theme function to actually do the theming.
  return theme_status_messages($variables);
}

/**
 * Implements theme_comment_post_forbidden().
 *
 * Updates login links to point to CAS.
 */
function uw_core_theme_comment_post_forbidden($variables) {
  $output = theme_comment_post_forbidden($variables);
  $output = _uw_core_theme_replace_with_cas_link($output);
  return $output;
}

/**
 * Helper function replaces links to "/user/login" with "/cas".
 *
 * @param string $output
 *   HTML for the login link.
 */
function _uw_core_theme_replace_with_cas_link($output) {
  return preg_replace(',( href="/[^"]+/)user/login(["?#]),', '$1cas$2', $output);
}

/**
 * Implements hook_page_alter().
 *
 * Force global header to display when they are empty.
 */
function uw_core_theme_page_alter(array &$page) {
  if (!isset($page["global_header"]) || empty($page["global_footer"])) {
    $page["global_header"] = array(
      '#region' => 'global_header',
      '#weight' => '-10',
      '#theme_wrappers' => array('region'),
    );
  }
}

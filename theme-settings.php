<?php

/**
 * @file
 * Functions relating to the theme settings.
 */

/**
 * Implements hook_form_system_theme_settings_alter().
 */
function uw_core_theme_form_system_theme_settings_alter(&$form, $form_state) {
  // Not directly adding to $form so we can make sure this appears first.
  $form_add['title_tag'] = array(
    '#type' => 'fieldset',
    '#title' => t('Title tag'),
  );
  $form_add['title_tag']['title_append'] = array(
    '#type' => 'textfield',
    '#title' => t('Append to title tag:'),
    '#default_value' => variable_get('uw_theme_title_append', ' | University of Waterloo'),
    '#description' => t("Generally, this should start with a space. Leave blank to append nothing."),
  );
  // Put our addition in front of the existing form.
  $form = $form_add + $form;
  $form['#submit'][] = 'uw_core_theme_set_custom_variable';
}

/**
 * Submit handler.
 */
function uw_core_theme_set_custom_variable($form, &$form_state) {
  if (isset($form_state['values']['title_append'])) {
    variable_set('uw_theme_title_append', $form_state['values']['title_append']);
  }
}

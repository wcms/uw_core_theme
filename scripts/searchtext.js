/**
 * @file
 */

(function ($) {
  Drupal.behaviors.jsSearch = {
    attach: function (context, settings) {
        $searchLabel = $('#uw-search-label').addClass('js');
        $searchInput = $('#uw-search-term');

        $searchLabel.click(function () {
            $searchInput.focus();
        });

        $searchInput.focus(function () {
            $searchLabel.addClass('element-invisible');
        });

        $searchInput.blur(function () {
          if (!$searchInput.val()) {
              $searchLabel.removeClass('element-invisible');
          }
        });
    }
  };
})(jQuery);

<?php

/**
 * @file
 * Template file for pages.
 */
?>
<div id="site" class="<?php print $classes; ?>"<?php print $attributes; ?>>
  <div id="skip">
    <a href="#main" class="element-invisible element-focusable"><?php print t('Skip to main'); ?></a>
    <a href="#footer" class="element-invisible element-focusable"><?php print t('Skip to footer'); ?></a>
  </div>
  <div id="page">
  <div id="header">
    <?php if (!empty($page['primaryheader'])): ?><div id="primary_header"><?php print render($page['primaryheader']); ?></div><?php
    endif; ?>

  </div><!--/header-->

  <div id="main" role="main">
    <h1><?php print $title ? $title : $site_name; ?></h1>
    <?php print $messages; ?>
    <?php print render($page['help']); ?>
    <?php if ($tabs): ?>
    <div class="tabs"><?php print render($tabs); ?></div>
    <?php endif; ?>
    <?php if ($action_links): ?>
    <ul class="action-links"><?php print render($action_links); ?></ul>
    <?php endif; ?>
    <?php print render($page['content']) ?>
  </div><!--/main-->

  <div id="footer" role="contentinfo"><?php print render($page['footer']); ?></div>
  </div><!--/page-->
</div><!--/site-->

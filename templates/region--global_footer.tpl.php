<?php

/**
 * @file
 * Default theme implementation to display a region.
 *
 * Available variables:
 * - $content: The content for this region, typically blocks.
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. The default values can be one or more of the following:
 *   - region: The current template type, i.e., "theming hook".
 *   - region-[name]: The name of the region with underscores replaced with
 *     dashes. For example, the page_top region would have a region-page-top class.
 * - $region: The name of the region variable as defined in the theme's .info file.
 *
 * Helper variables:
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 * - $is_admin: Flags true when the current user is an administrator.
 * - $is_front: Flags true when presented in the front page.
 * - $logged_in: Flags true when the current user is a logged-in member.
 *
 * @see template_preprocess()
 * @see template_preprocess_region()
 * @see template_process()
 */
?>

<div id="uw-footer" class="clearfix">
  <div id="uw-footer-address" about="//uwaterloo.ca/" typeof="v:VCard">
    <div class="element-hidden">
      <div property="v:fn">University of Waterloo</div>
      <div rel="v:org">
        <div property="v:organisation-name">University of Waterloo</div>
      </div>
      <div rel="v:geo">
        <div property="v:latitude">43.471468</div>
        <div property="v:longitude">-80.544205</div>
      </div>
    </div>
    <div rel="v:adr">
      <div property="v:street-address">200 University Avenue West</div>
      <div>
        <span property="v:locality">Waterloo</span>,
        <span property="v:region">ON</span>,
        <span property="v:country-name">Canada</span>&nbsp;
        <span property="v:postal-code">N2L 3G1</span>
      </div>
    </div>
    <div rel="v:tel">
      <a href="tel:+1-519-888-4567" property="rdf:value">+1 519 888 4567</a>
    </div>
  </div>
  <?php print $content; ?>
</div>

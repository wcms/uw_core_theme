<?php

/**
 * @file
 * Default theme implementation to display a region.
 *
 * Available variables:
 * - $content: The content for this region, typically blocks.
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. The default values can be one or more of the following:
 *   - region: The current template type, i.e., "theming hook".
 *   - region-[name]: The name of the region with underscores replaced with
 *     dashes. For example, the page_top region would have a region-page-top class.
 * - $region: The name of the region variable as defined in the theme's .info file.
 *
 * Helper variables:
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 * - $is_admin: Flags true when the current user is an administrator.
 * - $is_front: Flags true when presented in the front page.
 * - $logged_in: Flags true when the current user is a logged-in member.
 *
 * @see template_preprocess()
 * @see template_preprocess_region()
 * @see template_process()
 */
?>

<div id="uw-header" class="clearfix">
  <a id="uw-logo" href="//uwaterloo.ca/">University of Waterloo</a>
  <?php
  // Stop-gap until translation is properly implemented.
  global $language;
  $search = ($language->language === 'fr') ? 'Recherche' : 'Search';
  ?>
  <div id="uw-search">
    <form method="get" action="//uwaterloo.ca/search">
      <label id="uw-search-label" for="uw-search-term"><?php echo $search; ?></label>
      <input id="uw-search-term" type="text" size="31" tabindex="2" name="q" />
      <?php if (base_path() !== '/') : ?>
        <span id="uw-search-site-wrap">
          <label id="uw-search-site-label" for="uw-search-site" class="element-invisible"><?php echo t('search scope'); ?></label>
            <select name="q" id="uw-search-site" tabindex="1">
              <option value=""><?php echo t('all sites'); ?></option>
              <option value="site:<?php global $base_url; echo $base_url; ?>"><?php echo t('this site'); ?></option>
            </select>
        </span>
      <?php endif; ?>
      <input id="uw-search-submit" class="chevron-submit" type="submit" value="<?php echo $search; ?>" tabindex="2" />
      <input type="hidden" name="client" value="default_frontend" />
      <input type="hidden" name="proxystylesheet" value="default_frontend" />
      <input type="hidden" name="site" value="default_collection" />
    </form>
  </div>

  <nav class="nav-university" aria-label="university navigation">
    <ul class="global-menu">
      <li><a href="https://uwaterloo.ca/admissions/">Admissions</a></li>
      <li><a href="https://uwaterloo.ca/about/">About Waterloo</a></li>
      <li><a href="https://uwaterloo.ca/faculties-academics/">Faculties &amp; academics</a></li>
      <li><a href="https://uwaterloo.ca/offices-services/">Offices &amp; services</a></li>
      <li><a href="https://uwaterloo.ca/support/">Support Waterloo</a></li>
      <li><a href="https://uwaterloo.ca/coronavirus/">COVID-19</a></li>
    </ul>
  </nav>
  <?php print $content; ?>
</div>
